*** Settings ***
Library    SeleniumLibrary
Library    DebugLibrary

*** Variables ***
${URL}     https://ceape-prod.herokuapp.com/dash_users/sign_in

*** Keywords ***

Abrir o navegador
    Open Browser    browser=chrome
    Maximize Browser Window
    
Fechar o navegador
    Close Browser

Dado que acesso Dash board Ceape
    Go To    url=${URL}

Quando que insiro login valido 
    Sleep    3
    Debug
    ${CAMPO_EMAIL}    Get WebElement    id:dash_user_email
    Input Text      ${CAMPO_EMAIL}     superadmin@gmail.com
    ${CAMPO_PASS}    Get WebElement    id:password
    Input Text      ${CAMPO_PASS}     lD@V8b*N
    ${BTN_ENTRAR}    Get WebElement    css:[data-callback="invisibleRecaptchaSubmit"]
    Click Element     ${BTN_ENTRAR}
Entao sou redirecionado para login com sucesso
    Sleep    3
    ${VALIDA_LOGIN}    Get WebElement    id:js-page-content
    Wait Until Element Is Visible    ${VALIDA_LOGIN}
Dado que estou na tela principal
    Dado que acesso Dash board Ceape  
    Quando que insiro login valido  
    Entao sou redirecionado para login com sucesso    
        
Quando clico em Notificações
    ${CLIQUE_NOTIFICACAO}    Get WebElements    css:[class*="nav-link-text"]
    Click Element     ${CLIQUE_NOTIFICACAO}[1]

E seleciono a opção Adicionar notificação
   



