
***Settings***
Documentation       Configuração de execução.


Library    SeleniumLibrary

Resource           ${EXECDIR}/setup/default.robot

*** Variables ***
${URL}    https://ceape-prod.herokuapp.com/dash_users/sign_in

*** Keywords ***
Abrir o navegador
    Open Browser    browser=chromium
    # options=add_experimental_option("detach", True)
    Maximize Browser Window


Fechar o navegador
    Close Browser
