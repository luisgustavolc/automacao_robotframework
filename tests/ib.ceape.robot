*** Settings ***
Documentation
Resource    ${EXECDIR}/keywords/ib.ceape_kws.robot
#Resource         ../setup/setup.robot

Test Setup        Abrir o navegador
Test Teardown     Fechar o navegador


*** Test Cases ***

Caso de Testes 01 - Login Ceape
    [Documentation]
    ...
    [Tags]        menus
    Dado que acesso Dash board Ceape 
    Quando que insiro login valido
    Entao sou redirecionado para login com sucesso

Caso de Teste 02 - Funcionalidade: Adicionar notificação
     [Documentation]
     ...
     [Tags]       noti
     Dado que estou na tela principal
     Quando clico em Notificações
     E seleciono a opção Adicionar notificação
     E preencho o título com Transação de Cartão de Crédito Apropriada
     E preencho o subtítulo com Sua compra foi aprovada com sucesso!
     E preencho a descrição com:

     """
    Olá Caro cliente,
     
    Gostaríamos de informá-lo que sua transação no valor de R$1,99 realizada em casas bahia, foi aprovada com sucesso. O débito já foi efetuado em seu cartão de crédito com final 0101.

    """

    E preencho a data de envio com "data de envio"
    E clico em "Enviar para todos os usuários"
    E clico em "Cadastrar"
    Então vejo uma mensagem de sucesso




