


Instalando Debug
pip install -U robotframework-debuglibrary

Instalando Library Selenium
pip install -U robotframework-seleniumlibrary

Instalando Requests
pip install -U robotframework-requests


├── README.md
├── elements
│   ├── drogaraia
├── keywords
├── logs
├── requirements.txt
├── setup
│   └── setup.robot
└── tests

Abri navegador que deseja usar
Open Browser    browser=chrome

Fecha navegador
Close Browser

robot -d caminho_para_a_pasta_de_logs nome_do_arquivo.robot

robot -d logs amazon_testes.robot

execução após ajustes 
robot -d logs amazon_testes.robot -i tag tests/*



EXEMPLOS DE MAPEAMENTO
    ${CAMPO_EMAIL}    Get WebElement    id:dash_user_email
    
    Input Text      ${CAMPO_EMAIL}      superadmin@gmail.com

    ${BTN_ENTRAR}    Get WebElement    css:[data-callback="invisibleRecaptchaSubmit"]

    ${VALIDA_LOGRADOURO}    Get WebElement    xpath:.//tbody/tr[1]/td[1]
    ${VALIDA_BAIRRO}    Get WebElement    xpath:.//tbody/tr[1]/td[2]
    ${VALIDA_CIDADE}    Get WebElement    xpath:.//tbody/tr[1]/td[3]
    ${texto_logradouro}    Get Text    ${VALIDA_LOGRADOURO}
    ${texto_bairro}    Get Text    ${VALIDA_BAIRRO}
    ${texto_cidade}    Get Text    ${VALIDA_CIDADE}
    Log    ${texto_logradouro}
    Log    ${texto_bairro}
    Log    ${texto_cidade}